FROM centos:centos8

ENV LANG C.UTF-8

RUN dnf -y install \
    wget \
    java-11-openjdk-devel.x86_64

RUN wget --no-verbose -O /opt/synthea-with-dependencies.jar 'https://github.com/synthetichealth/synthea/releases/download/master-branch-latest/synthea-with-dependencies.jar'
