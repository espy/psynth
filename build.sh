#!/usr/bin/env bash

NOM="psynth"

OPTS=" "
OPTS="${OPTS} --file Dockerfile "
OPTS="${OPTS} --no-cache "
OPTS="${OPTS} -t ${NOM} "

docker build ${OPTS} .
